import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const firebase = admin.initializeApp();


export const onDeletePromo = functions.database.ref('/promo/{key}')
.onDelete(
   async (snapshot,context) => {
        const key = context.params.key
        await snapshot.ref.root.child("comentarios")
        .orderByChild("promoKey").equalTo(key).once('value', async (snapshot2) =>{
       await snapshot2.forEach((childSnapshot) =>{
            childSnapshot.ref.remove().then().catch()
        })  
            
        })
        
    }
)

export const deletePromoPhoto = functions.database.ref('/promo/{key}')
.onDelete(
    async (snapshot,context) => {
        firebase.storage().bucket().file(`/promo_images/${context.params.key}.jpeg`).delete().then()
        .catch(error => {console.log(`erro ao deletar a imagem relativa a promo ${context.params.key}`)})
    }
)