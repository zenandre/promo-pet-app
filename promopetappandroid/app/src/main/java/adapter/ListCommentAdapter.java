package adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.promopetapp.promo_pet_app_android.R;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import config.FirebaseConfig;
import de.hdodenhof.circleimageview.CircleImageView;
import model.Comment;
import model.Usuario;


public class ListCommentAdapter extends RecyclerView.Adapter<ListCommentAdapter.ItemListCommentHolder> {

    private List<Comment> comentarios;
    private DatabaseReference database = FirebaseConfig.getDatabase();


    public ListCommentAdapter(List<Comment> comentarios){
        this.comentarios = comentarios;
    }


    @NonNull
    @Override
    public ItemListCommentHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemListComment = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_comment,viewGroup,false);
        return new ItemListCommentHolder(itemListComment);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemListCommentHolder itemListCommentHolder, int i) {

        final Comment comentario = comentarios.get(i);
        final DatabaseReference userReference = database.child("user_data").child(comentario.getUserUid());
        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Usuario usuario = new Usuario();
                usuario = dataSnapshot.getValue(Usuario.class);
                itemListCommentHolder.username.setText(usuario.getNome().toString());
                Glide.with(itemListCommentHolder.itemView.getContext()).load(usuario.getPhotoUrl()).into(itemListCommentHolder.userThumbImage);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        itemListCommentHolder.commentText.setText(comentario.getText());
        Format formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        itemListCommentHolder.commentDate.setText(formatter.format(comentario.getDate()));
    }

    @Override
    public int getItemCount() {
        return comentarios.size();
    }




    public class ItemListCommentHolder extends RecyclerView.ViewHolder {
        private TextView username;
        private TextView commentDate;
        private TextView commentText;
        private CircleImageView userThumbImage;

        public ItemListCommentHolder(@NonNull View itemView){
            super(itemView);

            username = itemView.findViewById(R.id.username);
            commentDate = itemView.findViewById(R.id.comment_date);
            commentText = itemView.findViewById(R.id.comment_text);
            userThumbImage = itemView.findViewById(R.id.user_thumb_image);
        }
    }
}
