package adapter;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.promopetapp.promo_pet_app_android.R;
import com.promopetapp.promo_pet_app_android.activity.CommentActivity;
import com.promopetapp.promo_pet_app_android.activity.MainActivity;
import com.promopetapp.promo_pet_app_android.activity.ProfileActivity;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import config.FirebaseConfig;
import de.hdodenhof.circleimageview.CircleImageView;
import model.Comment;
import model.Promo;
import model.Usuario;

public class ListPromoAdapter extends RecyclerView.Adapter<ListPromoAdapter.ItemListPromoHolder>  {

    private List<Promo> promos;
    private DatabaseReference database = FirebaseConfig.getDatabase();
    private FirebaseAuth auth = FirebaseConfig.getFirebaseAuth();



    public ListPromoAdapter(List<Promo> promos) {
        this.promos = promos;
    }

    @NonNull
    @Override
    public ItemListPromoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemListPromo = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_promos,viewGroup,false);
        return new ItemListPromoHolder(itemListPromo);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemListPromoHolder itemListPromoHolder, int i) {

        final Promo promo = promos.get(i);
        final Dialog photo_modal;
        AlertDialog.Builder builder = new AlertDialog.Builder(itemListPromoHolder.itemView.getContext());
        LayoutInflater inflater = ( LayoutInflater) itemListPromoHolder.itemView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.photo_promo_modal,null);
        MapsInitializer.initialize(itemListPromoHolder.itemView.getContext());
        ImageView imageViewModal = view.findViewById(R.id.promo_photo_modal);
        ImageView close_icon = view.findViewById(R.id.close_modal_icon);
        Glide.with(itemListPromoHolder.itemView.getContext()).load(Uri.parse(promo.getPhotoUrl())).into(imageViewModal);
        builder.setView(view);
        photo_modal = builder.create();
        final Dialog map_modal = new Dialog(itemListPromoHolder.itemView.getContext());
        View map_modal_view = inflater.inflate(R.layout.map_modal,null);
        ImageView close_map_modal = map_modal_view.findViewById(R.id.close_map_modal);
        close_map_modal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map_modal.dismiss();
            }
        });
        map_modal.setContentView(map_modal_view);
        map_modal.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        photo_modal.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        close_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photo_modal.dismiss();
            }
        });

        View.OnClickListener clickListenerImagePromo = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photo_modal.show();
            }
        };
        View.OnClickListener clickListenerOpenMap = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map_modal.show();
                MapView mapView = map_modal.findViewById(R.id.map_view);
                MapsInitializer.initialize(itemListPromoHolder.itemView.getContext());
                mapView.onCreate(map_modal.onSaveInstanceState());
                mapView.onResume();
                mapView.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                    LatLng latLng = new LatLng(promo.getLocal().getCoords().getLatitude(),promo.getLocal().getCoords().getLongitude());
                    googleMap.addMarker(new MarkerOptions().position(latLng).title(promo.getLocal().getName()));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
                    }
                });

            }
        };
        if (!promo.getUserUid().equals(auth.getCurrentUser().getUid())){
            itemListPromoHolder.icMenuItemPromo.setVisibility(View.GONE);
        }
        itemListPromoHolder.icMenuItemPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popUp = new PopupMenu(itemListPromoHolder.itemView.getContext(),v);
                popUp.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.deleteMenu :
                                final DatabaseReference deletePromoReference = database.child("promo").child(promo.getKey());
                                deletePromoReference.removeValue()
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(itemListPromoHolder.itemView.getContext(),"Erro ao deletar postagem",Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(itemListPromoHolder.itemView.getContext(),"Postagem deletada",Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                break;
                        }
                        return false;
                    }
                });
                MenuInflater menuInflater = popUp.getMenuInflater();
                menuInflater.inflate(R.menu.item_promo_menu,popUp.getMenu());
                popUp.show();
            }
        });

        itemListPromoHolder.description.setText(promo.getDescricao());
        Format formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        itemListPromoHolder.persistDateTime.setText(formatter.format(promo.getDataCadastro()));
        itemListPromoHolder.price.setText("R$ " + promo.getPreco().toString());
        Uri uri = Uri.parse(promo.getPhotoUrl());
        Glide.with(itemListPromoHolder.itemView.getContext())
                .load(uri).into(itemListPromoHolder.photo);
        itemListPromoHolder.photo.setOnClickListener(clickListenerImagePromo);
        itemListPromoHolder.place.setText(promo.getLocal().getName());

        if (promo.getDataInicio() != null) {
            itemListPromoHolder.startDate.setText(promo.getDataInicio());
        }
        else {

        }
        if (promo.getDataFim() != null) {
            itemListPromoHolder.finalDate.setText(promo.getDataFim());
        }
        else{

        }
        itemListPromoHolder.placeView.setOnClickListener(clickListenerOpenMap);

        if (promo.getCurtidas() != null){
            itemListPromoHolder.countLikes.setText(String.valueOf(promo.getCurtidas().size()));
        }
        if (promo.getListaNegativacao() != null){
            itemListPromoHolder.countDislikes.setText(String.valueOf(promo.getListaNegativacao().size()));
        }

        itemListPromoHolder.icComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(itemListPromoHolder.itemView.getContext(), CommentActivity.class);
                intent.putExtra("promo",promo);
                itemListPromoHolder.itemView.getContext().startActivity(intent);
            }
        });

        final DatabaseReference commentsReference = database.child("comentarios");
        final List<Comment> comentarios = new ArrayList<>();
        commentsReference.orderByChild("promoKey").equalTo(promo.getKey()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                comentarios.add(dataSnapshot.getValue(Comment.class));
                if (!comentarios.isEmpty()){
                    itemListPromoHolder.countComments.setText(String.valueOf(comentarios.size()));
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        itemListPromoHolder.icLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DatabaseReference likesReference = database.child("promo").child(promo.getKey()).child("curtidas");
                String key = likesReference.push().getKey();
                likesReference.child(key).setValue(auth.getCurrentUser().getUid()).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(itemListPromoHolder.itemView.getContext(),"Erro ao curtir postagem",Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(itemListPromoHolder.itemView.getContext(),"Postagem curtida",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        itemListPromoHolder.icDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DatabaseReference dislikesReference = database.child("promo").child(promo.getKey()).child("listaNegativacao");
                String key = dislikesReference.push().getKey();
                dislikesReference.child(key).setValue(auth.getCurrentUser().getUid()).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(itemListPromoHolder.itemView.getContext(),"Erro ao negativar postagem",Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(itemListPromoHolder.itemView.getContext(),"Postagem negativada",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        final DatabaseReference userReference = database.child("user_data").child(promo.getUserUid());
        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Usuario user = dataSnapshot.getValue(Usuario.class);
                itemListPromoHolder.userPromo.setText(user.getNome());
                Glide.with(itemListPromoHolder.itemView).load(Uri.parse(user.getPhotoUrl())).into(itemListPromoHolder.userProfilePhoto);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return promos.size();
    }

    public class ItemListPromoHolder extends RecyclerView.ViewHolder {

        TextView description;
        TextView place;
        ImageView photo;
        CircleImageView userProfilePhoto;
        TextView startDate;
        TextView finalDate;
        TextView persistDateTime;
        TextView countLikes;
        TextView countDislikes;
        TextView countComments;
        TextView price;
        TextView userPromo;
        LinearLayout placeView;
        ImageView icLike;
        ImageView icDislike;
        ImageView icComment;
        ImageView icMenuItemPromo;

        public ItemListPromoHolder(@NonNull View itemView) {
            super(itemView);

            description = itemView.findViewById(R.id.promo_descricao);
            place = itemView.findViewById(R.id.place_label_promo);
            photo = itemView.findViewById(R.id.promo_image);
            userProfilePhoto = itemView.findViewById(R.id.user_thumb);
            startDate = itemView.findViewById(R.id.start_date_promo);
            finalDate = itemView.findViewById(R.id.final_date_promo);
            persistDateTime = itemView.findViewById(R.id.promo_persist_date_header);
            countLikes = itemView.findViewById(R.id.count_likes);
            countDislikes = itemView.findViewById(R.id.count_dislike);
            countComments = itemView.findViewById(R.id.count_comment);
            price = itemView.findViewById(R.id.promo_price);
            userPromo = itemView.findViewById(R.id.promo_user_header);
            placeView = itemView.findViewById(R.id.linearLayout3);
            icLike = itemView.findViewById(R.id.ic_like);
            icDislike = itemView.findViewById(R.id.ic_dislike);
            icComment = itemView.findViewById(R.id.ic_comment);
            icMenuItemPromo = itemView.findViewById(R.id.item_promo_menu);
        }
    }



}
