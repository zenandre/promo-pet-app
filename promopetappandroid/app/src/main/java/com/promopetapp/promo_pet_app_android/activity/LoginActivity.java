package com.promopetapp.promo_pet_app_android.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.promopetapp.promo_pet_app_android.R;

import config.FirebaseConfig;
import helper.HelperPermisions;

public class LoginActivity extends AppCompatActivity {

     private TextInputEditText inputEmail, inputSenha;
     private FirebaseAuth fireAuth;
    private Dialog loading;
    private String[] permissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        HelperPermisions.validatePermissions(permissions,this);

        inputEmail = findViewById(R.id.inputEmail);
        inputSenha = findViewById(R.id.inputPassword);
        fireAuth = FirebaseConfig.getFirebaseAuth();

        loading = configLoading();
        loading.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    public Dialog configLoading() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.progress_layout,null));
        return builder.create();
    }

    public void signUpEmailPasswordFirebase(String email,String password){

        fireAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(
                this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){

                        } else {
                            try {
                                throw task.getException();
                            }catch (FirebaseAuthInvalidCredentialsException e) {
                                Toast.makeText(LoginActivity.this,"Senha muito fraca!",Toast.LENGTH_SHORT);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        );
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser usuarioLogado = fireAuth.getCurrentUser();
        if (usuarioLogado != null){
            goToMain();
        }
    }

    public void goToMain(){
        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);
    }

    public void signInEmailPassword(View view){
        loading.show();
        String textEmail = inputEmail.getText().toString();
        String textSenha = inputSenha.getText().toString();
        if( !textEmail.isEmpty() && !textSenha.isEmpty()){
            this.fireAuth = FirebaseConfig.getFirebaseAuth();
            this.fireAuth.signInWithEmailAndPassword(textEmail,textSenha).addOnCompleteListener(
                    new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                loading.dismiss();
                                goToMain();
                            }else {
                                loading.dismiss();
                                Toast.makeText(LoginActivity.this,
                                        "Erro ao autenticar usuário!",
                                         Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
            );
        } else {
        Toast.makeText(LoginActivity.this,
                "Preencha todos os campos solicitados",
                Toast.LENGTH_SHORT).show();
    }
    }

    public void signUpEmailPassword(View view){
        String textEmail = inputEmail.getText().toString();
        String textSenha = inputSenha.getText().toString();

        if( !textEmail.isEmpty() && !textSenha.isEmpty()){
            this.signUpEmailPasswordFirebase(textEmail,textEmail);
        }else {
            Toast.makeText(LoginActivity.this,
                    "Preencha todos os campos solicitados",
                    Toast.LENGTH_SHORT).show();
        }

    }
}
