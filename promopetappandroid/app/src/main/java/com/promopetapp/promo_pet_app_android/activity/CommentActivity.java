package com.promopetapp.promo_pet_app_android.activity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.promopetapp.promo_pet_app_android.R;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapter.ListCommentAdapter;
import config.FirebaseConfig;
import de.hdodenhof.circleimageview.CircleImageView;
import model.Comment;
import model.Promo;
import model.Usuario;

public class CommentActivity extends AppCompatActivity {

    private Promo promo;
    private List<Comment> comments = new ArrayList<Comment>();
    private DatabaseReference database = FirebaseConfig.getDatabase();
    private FirebaseAuth auth = FirebaseConfig.getFirebaseAuth();
    private Toolbar toolbar;
    private RecyclerView commentsRecyclerview;
    private TextInputEditText newCommentText;
    private ImageView sendButton;

    private TextView usernamePromo;
    private CircleImageView userThumbPromo;
    private TextView promoDescricao;
    private TextView promoDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        promo  = (Promo) getIntent().getSerializableExtra("promo");

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Comentários");
        setSupportActionBar( toolbar );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        usernamePromo = findViewById(R.id.username_promo);
        userThumbPromo = findViewById(R.id.user_thumb_image);
        promoDescricao = findViewById(R.id.promo_descricao);
        promoDate = findViewById(R.id.promo_date);

        final DatabaseReference userReference = database.child("user_data").child(promo.getUserUid());
        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                usernamePromo.setText(usuario.getNome());
                Glide.with(CommentActivity.this).load(usuario.getPhotoUrl()).into(userThumbPromo);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        promoDescricao.setText(promo.getDescricao());
        Format formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        promoDate.setText(formatter.format(promo.getDataCadastro()));

        newCommentText = findViewById(R.id.input_comment);
        sendButton = findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComment();
            }
        });

        final ListCommentAdapter listCommentAdapter = new ListCommentAdapter(comments);
        commentsRecyclerview = findViewById(R.id.comment_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        commentsRecyclerview.setLayoutManager(layoutManager);
        commentsRecyclerview.setHasFixedSize(true);
        commentsRecyclerview.addItemDecoration(new DividerItemDecoration(this,LinearLayout.VERTICAL));
        commentsRecyclerview.setAdapter(listCommentAdapter);

        final DatabaseReference commentsReference = database.child("comentarios");
        commentsReference.orderByChild("promoKey").equalTo(promo.getKey()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                comments.add(dataSnapshot.getValue(Comment.class));
                listCommentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void sendComment(){

        if (newCommentText.getText().toString() != null && newCommentText.getText().toString() != "") {
            final DatabaseReference commentReference = database.child("comentarios");
            final Comment newComment = new Comment();
            newComment.setKey(commentReference.push().getKey());
            newComment.setText(newCommentText.getText().toString());
            newComment.setDate(Calendar.getInstance().getTime());
            newComment.setUserUid(auth.getCurrentUser().getUid());
            newComment.setPromoKey(promo.getKey());

            commentReference.child(newComment.getKey()).setValue(newComment).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(CommentActivity.this,"Comentário adicionado",Toast.LENGTH_SHORT).show();
                    newCommentText.setText("");

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(CommentActivity.this,"Erro ao tentar adicionar comentário",Toast.LENGTH_SHORT).show();
                }
            });

        }
        else {
            Toast.makeText(CommentActivity.this,"Prencha o campo comentário",Toast.LENGTH_SHORT).show();
        }

    }
}
