package com.promopetapp.promo_pet_app_android.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.promopetapp.promo_pet_app_android.R;

import java.util.ArrayList;
import java.util.List;

import adapter.ListPromoAdapter;
import config.FirebaseConfig;
import model.Promo;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth fireauth;
    private DatabaseReference promosReference;
    private RecyclerView promoListRecyclerView;
    private List<Promo> promos;
    private Dialog loading;
    private ChildEventListener promoChildEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loading = configLoading();
        loading.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        loading.show();

        promos = new ArrayList<Promo>();
        final ListPromoAdapter listPromoAdapter = new ListPromoAdapter(promos);
        promoListRecyclerView = findViewById(R.id.promo_list_recyclerview);
        RecyclerView.LayoutManager promoListLayoutManager = new LinearLayoutManager(getApplicationContext());
        promoListRecyclerView.setLayoutManager(promoListLayoutManager);
        promoListRecyclerView.setHasFixedSize(true  );
        promoListRecyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayout.VERTICAL));
        promoListRecyclerView.setAdapter(listPromoAdapter);
        fireauth = FirebaseConfig.getFirebaseAuth();
        promosReference = FirebaseConfig.getDatabase().child("promo");
        promoChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                promos.add(dataSnapshot.getValue(Promo.class));
                listPromoAdapter.notifyDataSetChanged();
                loading.dismiss();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                promos.set(promos.indexOf(dataSnapshot.getValue(Promo.class)),dataSnapshot.getValue(Promo.class));
                listPromoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                promos.remove(dataSnapshot.getValue(Promo.class));
                listPromoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                listPromoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listPromoAdapter.notifyDataSetChanged();
            }
        };
        promosReference.addChildEventListener(promoChildEventListener);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Promos");
        setSupportActionBar( toolbar );

    }

    public Dialog configLoading() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.progress_layout,null));
        return builder.create();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser usuarioLogado = fireauth.getCurrentUser();
        if (usuarioLogado == null){
            goToLogin();
        }
    }

    public void goToLogin(){
        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.exitMenu :
                    logout();
                break;
            case R.id.profileMenu :
                goToProfile();
        }
        return super.onOptionsItemSelected(item);
    }

    public void logout(){
        try {
            finish();
            promosReference.removeEventListener(promoChildEventListener);
            goToLogin();
            fireauth.signOut();
        } catch (Exception e){

        }

    }

    public void goToProfile(){
        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(intent);
    }

    public void goToNewPromo(View view){
        Intent intent = new Intent(MainActivity.this, NewPromoActivity.class);
        startActivity(intent);
    }
}
