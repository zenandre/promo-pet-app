package com.promopetapp.promo_pet_app_android.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.promopetapp.promo_pet_app_android.R;

import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import config.FirebaseConfig;
import model.Local;
import model.Promo;

public class NewPromoActivity extends AppCompatActivity {

    private DatabaseReference database = FirebaseConfig.getDatabase().child("promo");
    private FirebaseAuth auth = FirebaseConfig.getFirebaseAuth();
    private StorageReference storage = FirebaseConfig.getFirebaseStorage();
    private PlacesClient placesClient;
    private List<Place.Field> placeFields = Arrays.asList(Place.Field.ID,Place.Field.NAME,Place.Field.ADDRESS,Place.Field.LAT_LNG);
    private AutocompleteSupportFragment autocompleteSupportFragment;

    private ImageView promoPhoto;
    private TextView textViewDataInicio,textViewDataFinal;
    private TextInputEditText descricaoInput,precoInput;
    private Promo promo = new Promo();
    private Toolbar toolbar;

    private static final int CAMERA_REQEST_CODE = 100;
    private static final int GALERY_REQEST_CODE = 200;
    private byte[] imageData;

    private Dialog loading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_promo);

        descricaoInput = findViewById(R.id.input_descricao);
        textViewDataInicio = findViewById(R.id.tv_data_inicial);
        textViewDataFinal = findViewById(R.id.tv_data_final);
        precoInput = findViewById(R.id.preco_input);
        promoPhoto = findViewById(R.id.new_promo_image);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Nova Promo");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        initPlaces();
        setupPlacesAutocomplete();
        loading = configLoading();
        loading.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    public Dialog configLoading() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.progress_layout,null));
        return builder.create();
    }

    public void setupPlacesAutocomplete(){
        autocompleteSupportFragment = (AutocompleteSupportFragment) getSupportFragmentManager()
                .findFragmentById(R.id.auto_complete_place_fragment);
        autocompleteSupportFragment.setPlaceFields(placeFields);
        autocompleteSupportFragment.setHint("Pesquise o local");
        autocompleteSupportFragment.setCountry("br");
        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                Local local = new Local(place);
                local.setCoords(new model.LatLng(place.getLatLng()));
                promo.setLocal(local);
            }

            @Override
            public void onError(@NonNull Status status) {

            }
        });
    }

    public void initPlaces(){
        Places.initialize(this,getString(R.string.places_api_key));
        placesClient = Places.createClient(this);
    }

    private void getCameraImage(){
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(i, CAMERA_REQEST_CODE);
        }
    }

    private void getGaleryImage(){
        Intent i = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(i, GALERY_REQEST_CODE);
        }
    }

    public void openImageOriginSelector(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Imagem da Promo");
        builder.setMessage("Seleciode a origem da imagem - Câmera ou Galeria");
        builder.setPositiveButton("Câmera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getCameraImage();
            }
        });
        builder.setNegativeButton("Galeria", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getGaleryImage();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            Bitmap image = null;

            try {
                switch (requestCode) {
                    case CAMERA_REQEST_CODE :
                        image = (Bitmap) data.getExtras().get("data");
                        break;

                    case GALERY_REQEST_CODE :
                        Uri imagePath = data.getData();
                        image = MediaStore.Images.Media.getBitmap(getContentResolver(),imagePath);
                        break;
                }

                if (image != null){
                    this.promoPhoto.setImageBitmap(image);
                    ByteArrayOutputStream baos =  new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG,70,baos);
                    this.imageData = baos.toByteArray();

                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public void savePromo(View view){

        if(this.validateFields(this.promo)){
            loading.show();
            preparePromoToSave();
            promo.setKey(database.push().getKey());
            database.child(promo.getKey()).setValue(promo).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    final StorageReference imageReference = storage.child("promo_images").child(promo.getKey()+ ".jpeg");
                    UploadTask uploadTask = imageReference.putBytes(imageData);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            loading.dismiss();
                            Toast.makeText(NewPromoActivity.this,
                                    "Falha no upload da imagem",
                                    Toast.LENGTH_LONG).show();
                        }
                    })
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    imageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            promo.setPhotoUrl(uri.toString());
                                            database.child(promo.getKey()).setValue(promo).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Intent intent = new Intent(NewPromoActivity.this, MainActivity.class);
                                                    startActivity(intent);
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    loading.dismiss();
                                                    Toast.makeText(NewPromoActivity.this,
                                                            "Falha ao finalizar salvamento",
                                                            Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            loading.dismiss();
                                            Toast.makeText(NewPromoActivity.this,
                                                    "Falha ao requisitar URL da imagem",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    loading.dismiss();
                }
            });
        }



    }


    public void chooseDate(View view){
        final TextView tv = (TextView) view;
        Calendar actualDate = Calendar.getInstance();
        Integer month = actualDate.get(Calendar.MONTH);
        Integer day = actualDate.get(Calendar.DAY_OF_MONTH);
        Integer year = actualDate.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(NewPromoActivity.this);
        datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener(){

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        tv.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                    }
                });
        datePickerDialog.show();
    }


    public boolean validateFields(Promo promo){
        List<String> listFields = new ArrayList<String>();

        if (imageData == null){
            listFields.add("É necessário adicionar uma foto");
        }
        if (descricaoInput.getText().toString().isEmpty()){
            listFields.add("O campo descrição é obrigatório");
        }
        if (precoInput.getText().toString().isEmpty()){
            listFields.add("O preço da promo é obrigtório");
        }
        if (promo.getLocal() == null ){
            listFields.add("Local é obrigatório");
        }

        if (listFields.isEmpty()){
            return true;
        }

        for (String field: listFields){
            Toast.makeText(NewPromoActivity.this,field,Toast.LENGTH_LONG).show();
        }
        return false;
    }

    public void preparePromoToSave(){
        this.promo.setDescricao(descricaoInput.getText().toString());
        this.promo.setPreco(precoInput.getText().toString());
        if (!textViewDataInicio.getText().toString().equals("Início")){
            this.promo.setDataInicio(textViewDataInicio.getText().toString());
        }
        if (!textViewDataFinal.getText().toString().equals("Final")){
            this.promo.setDataFim(textViewDataFinal.getText().toString());
        }
        this.promo.setDataCadastro(Calendar.getInstance().getTime());
        this.promo.setUserUid(auth.getCurrentUser().getUid());
    }

}
