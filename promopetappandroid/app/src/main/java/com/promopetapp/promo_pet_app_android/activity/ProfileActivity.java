package com.promopetapp.promo_pet_app_android.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.promopetapp.promo_pet_app_android.R;

import java.io.ByteArrayOutputStream;

import config.FirebaseConfig;
import de.hdodenhof.circleimageview.CircleImageView;
import helper.HelperPermisions;
import model.Usuario;

public class ProfileActivity extends AppCompatActivity {

    private String[] permissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private Usuario usuario;
    private TextInputEditText nameInput,emailInput,phoneInput;
    private FirebaseAuth auth;
    private DatabaseReference database;
    private StorageReference storage;
    private static final int CAMERA_REQEST_CODE = 100;
    private static final int GALERY_REQEST_CODE = 200;
    private CircleImageView circleImageView;
    private ProgressBar progressUploadPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        HelperPermisions.validatePermissions(permissions,this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Dados do usuário");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        auth = FirebaseConfig.getFirebaseAuth();
        storage = FirebaseConfig.getFirebaseStorage();
        database = FirebaseConfig.getDatabase();
        nameInput = findViewById(R.id.name_input);
        emailInput = findViewById(R.id.email_input);
        phoneInput = findViewById(R.id.phone_input);
        circleImageView = findViewById(R.id.profile_image);
        progressUploadPhoto = findViewById(R.id.progressPhotoUpload);
        progressUploadPhoto.setVisibility(View.GONE);

        getUserData();

    }

    public void getUserData(){
        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usuario = dataSnapshot.getValue(Usuario.class);
                if (usuario != null){
                    showUserDataOnActivity();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        database.child("user_data").child(auth.getCurrentUser().getUid()).addValueEventListener(userListener);

    }

    public void showUserDataOnActivity(){
        nameInput.setText(usuario.getNome());
        emailInput.setText(usuario.getEmail());
        phoneInput.setText(usuario.getPhoneNumber());
        if (usuario.getPhotoUrl() != null) {
            Uri uri = Uri.parse(usuario.getPhotoUrl());
            Glide.with(ProfileActivity.this)
                    .load(uri).into(circleImageView);
            circleImageView.setVisibility(View.VISIBLE);
        }
        else {
            circleImageView.setImageResource(R.drawable.default_photo);
            circleImageView.setVisibility(View.VISIBLE);
        }
    }

    public void getImageFromStorage(Uri uri){

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int permissionResult : grantResults){
            if (permissionResult == PackageManager.PERMISSION_DENIED){
                alertDeniedPermission();
            }
        }
    }

    private void alertDeniedPermission(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permisssões necessárias");
        builder.setCancelable(false);
        builder.setMessage("Para atualizar o perfil de usuário, é necessário aceitar as permissões para acesso de camera e storage");
        builder.setPositiveButton("Entendi", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void getCameraImage(){
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(i, CAMERA_REQEST_CODE);
        }
    }

    private void getGaleryImage(){
        Intent i = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(i, GALERY_REQEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            Bitmap image = null;

            try {
                    switch (requestCode) {
                        case CAMERA_REQEST_CODE :
                            image = (Bitmap) data.getExtras().get("data");
                            break;

                        case GALERY_REQEST_CODE :
                            Uri imagePath = data.getData();
                            image = MediaStore.Images.Media.getBitmap(getContentResolver(),imagePath);
                            break;
                    }

                    if (image != null){
                        progressUploadPhoto.setVisibility(View.VISIBLE);
                        circleImageView.setAlpha(0.6f);
                        ByteArrayOutputStream baos =  new ByteArrayOutputStream();
                        image.compress(Bitmap.CompressFormat.JPEG,70,baos);
                        byte[] imageData = baos.toByteArray();

                        final StorageReference imageReference = storage.child("user_avatar")
                                .child(auth.getCurrentUser().getUid().toString() + ".jpeg");

                        UploadTask uploadTask = imageReference.putBytes(imageData);
                        uploadTask.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                progressUploadPhoto.setVisibility(View.GONE);
                                circleImageView.setAlpha(1f);
                                Toast.makeText(ProfileActivity.this,
                                        "Falha no upload da imagem",
                                        Toast.LENGTH_SHORT).show();
                            }
                        })
                                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                       imageReference.getDownloadUrl()
                                               .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                   @Override
                                                   public void onSuccess(Uri uri) {
                                                       database.child("user_data")
                                                               .child(usuario.getUid())
                                                               .child("photoUrl")
                                                               .setValue(uri.toString())
                                                               .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                   @Override
                                                                   public void onComplete(@NonNull Task<Void> task) {
                                                                       progressUploadPhoto.setVisibility(View.GONE);
                                                                       circleImageView.setAlpha(1f);
                                                                       Toast.makeText(ProfileActivity.this,
                                                                               "Sucesso ao atualizar imagem do perfil",
                                                                               Toast.LENGTH_SHORT).show();
                                                                   }
                                                               });
                                                   }
                                               });




                                    }
                                });
                    }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void openImageOriginSelector(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Imagem do perfil");
        builder.setMessage("Seleciode a origem da imagem - Câmera ou Galeria");
        builder.setPositiveButton("Câmera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getCameraImage();
            }
        });
        builder.setNegativeButton("Galeria", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getGaleryImage();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void updateUserData(View view){
        prepareUserToUpdate();
        database.child("user_data")
                .child(usuario.getUid())
                .setValue(usuario)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(ProfileActivity.this,
                                "Dados atualizados com sucesso",
                                Toast.LENGTH_SHORT).show();
                        goToMain();
                    }

                });

    }

    public void prepareUserToUpdate(){
        usuario.setNome(nameInput.getText().toString());
        usuario.setEmail(emailInput.getText().toString());
        usuario.setPhoneNumber(phoneInput.getText().toString());
    }

    public void goToMain(){
        Intent intent = new Intent(ProfileActivity.this,MainActivity.class);
        startActivity(intent);
    }

}
