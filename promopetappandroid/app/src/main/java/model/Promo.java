package model;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.model.Place;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Promo implements Serializable {
    private String key;
    private String descricao;
    private String photoUrl;
    private String fileNameStorage;
    private String preco;
    private String userUid;
    private boolean valid;
    private String dataInicio;
    private String dataFim;
    private Date dataCadastro;
    private Local local;
    private HashMap<String,String> curtidas;
    private HashMap<String,String> listaNegativacao;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getFileNameStorage() {
        return fileNameStorage;
    }

    public void setFileNameStorage(String fileNameStorage) {
        this.fileNameStorage = fileNameStorage;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
       this.local = local;
    }

    public HashMap<String,String> getCurtidas() {
        return curtidas;
    }

    public void setCurtidas(HashMap<String,String> curtidas) {
        this.curtidas = curtidas;
    }

    public HashMap<String,String> getListaNegativacao() {
        return listaNegativacao;
    }

    public void setListaNegativacao(HashMap<String,String> listaNegativacao) {
        this.listaNegativacao = listaNegativacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Promo promo = (Promo) o;
        return Objects.equals(key, promo.key);
    }

    @Override
    public int hashCode() {

        return Objects.hash(key);
    }
}
