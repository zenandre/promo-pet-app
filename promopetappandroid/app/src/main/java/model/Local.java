package model;

import com.google.android.libraries.places.api.model.Place;

import java.io.Serializable;

public class Local implements Serializable {
    private String address;
    private String id;
    private String name;
    private LatLng coords;
    public Local(){

    }

    public Local(String address, String id, String name) {
        this.address = address;
        this.id = id;
        this.name = name;
    }

    public Local(Place place){
        this.address = place.getAddress();
        this.id = place.getId();
        this.name = place.getName();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getCoords() {
        return coords;
    }

    public void setCoords(LatLng coords) {
        this.coords = coords;
    }
}
