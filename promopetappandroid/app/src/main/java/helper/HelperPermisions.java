package helper;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class HelperPermisions {

    public static boolean validatePermissions(String[] permissions, Activity activity){
        if (Build.VERSION.SDK_INT >= 23){

            List<String> permissionsToRequest = new ArrayList<>();

            for (String permission : permissions){
                if (!hasPermission(permission,activity)) permissionsToRequest.add(permission);
            }

            if (permissionsToRequest.isEmpty()) return true;

            String[] permissionsToRequestArray = new String[permissionsToRequest.size()];
            permissionsToRequest.toArray(permissionsToRequestArray);

            ActivityCompat.requestPermissions(activity,permissionsToRequestArray    ,1);

        }

        return true;
    }

    private static boolean hasPermission(String permission, Activity activity){
        if (ContextCompat.checkSelfPermission(activity,permission) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }
}
