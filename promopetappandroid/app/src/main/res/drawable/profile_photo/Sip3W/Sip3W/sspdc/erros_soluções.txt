

Problema:
'keytool' n�o � reconhecido como um comando interno ou externo, um programa oper�vel ou um arquivo em lotes.

Solu��o:
1� Passo  Verificar vers�o atual do java:
  - No prompt do Dos executar: java -version
  - NO explorer verificar caminho do java(jre) corrente: Ex: C:\Arquivos de programas\Java\jre1.8.0_121
2� Passo Incluir caminho na vari�vel de ambiente PATH

  - Bot�o direito em Meu Computador -> Propriedades -> Avan�ado -> Vari�vies de ambiente -> Vari�veis do sistema 
     - Editar "Path"
     - incluir a seguinte linha de exemplo: C:\Arquivos de programas\Java\jre1.8.0_121\bin
p.s.: os caminhos s�o separados por ";"


