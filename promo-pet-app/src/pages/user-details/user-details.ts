import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { Usuario } from './../../model/usuario.model';

@IonicPage()
@Component({
  selector: 'page-user-details',
  templateUrl: 'user-details.html',
})
export class UserDetailsPage {
  phoneNumber: string;
  photoUrl: string = "../assets/imgs/default-avatar.jpg";
  email: string;
  displayName: string;
  foto: string;
  cameraOn: boolean = false;
  user: firebase.User;
  

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public camera: Camera) {
  }

  ionViewDidLoad() {
    this.user = firebase.auth().currentUser;
    console.log(this.user)
    this.displayName = this.user.displayName;
    this.email = this.user.email;
    if (this.user.photoURL != null && this.user.photoURL != "" && this.user.photoURL != undefined){
    this.photoUrl = this.user.photoURL;
    }
    this.phoneNumber = this.user.phoneNumber;
  }

  saveUserOnDatabase(user: firebase.User){

    let usuario: Usuario = new Usuario();
    usuario.id = user.uid
    usuario.nome = user.displayName
    usuario.photoUrl = user.photoURL

    firebase.database().ref("user_data/"+ usuario.id).set(usuario)
    .then( () => {})
    .catch( error => {console.log(error)})
    
  }

  updateUser(){    
    if (this.email != null || this.email != ""|| this.email != undefined){
      this.user.updateEmail(this.email).then().catch();
    }
    this.user.updateProfile({
      displayName: this.displayName,
      photoURL: this.photoUrl
    })
    .then( () => {
      this.saveUserOnDatabase(firebase.auth().currentUser)
      this.navCtrl.setRoot("TimelinePage") 
      
    })
    .catch(error => {})
  }

  getGaleryPicture(){    
    this.camera.getPicture(this.getGaleryOptions()).then((imageData) => { 
      this.uploadFoto('data:image/jpeg;base64,' + imageData)   
    }
    ).catch(
      error => { console.log(error)}
    );
  }

  getCameraPicture(){    
    this.camera.getPicture(this.getCameraOptions()).then((imageData) => { 
     this.uploadFoto('data:image/jpeg;base64,' + imageData)   
    }
    ).catch(
      error => {console.log(error)}
    );
  }

  uploadFoto(foto){
   let user = firebase.auth().currentUser 
   const uploadTask = firebase.storage().ref(`user_avatar/${user.uid}.jpeg`).putString(foto,'data_url')
   uploadTask.on(
     'state_changed',
     (snapshot) => {
     },
     (error) =>{
     },
     () =>{
     firebase.storage().ref('user_avatar').child(`${user.uid}.jpeg`).getDownloadURL().then(
      data => {
        this.photoUrl = data;
      }
    )
    .catch(
      (error) => {
      }
    )
     }
   )
    
  }

getCameraOptions(){
  const cameraOptions: CameraOptions = {
    quality: 50,
    sourceType: this.camera.PictureSourceType.CAMERA,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    allowEdit: true,
    correctOrientation: true
  }
  return cameraOptions;
}

getGaleryOptions(){
  const galeryOptions: CameraOptions = {
    quality: 50,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    allowEdit: true,
    correctOrientation: true
  }
  return galeryOptions;
}
   
}
