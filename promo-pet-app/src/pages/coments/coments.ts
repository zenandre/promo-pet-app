import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Promocao } from './../../model/promocao.model';
import firebase from 'firebase';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Comentario } from '../../model/comentario.model';


@IonicPage()
@Component({
  selector: 'page-coments',
  templateUrl: 'coments.html',
})
export class ComentsPage implements OnInit {

  formComent: FormGroup;
  promo: Promocao;
  comentarios: Comentario[] = new Array<Comentario>();
  comentario: Comentario = new Comentario();

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public fb: FormBuilder) {

                this.promo = this.navParams.get('promo')
                this.getComentarios()
             
  }

  ngOnInit(){

    this.formComent = this.fb.group({
      comment: ['',[Validators.required]]
    })

    
  }

  getComentarios(){
    this.comentarios = new Array<Comentario>()
    
    firebase.database().ref('comentarios').orderByChild('promoKey').equalTo(this.promo.key).on(
      'child_added',
      snapshot => {
        let comentario: Comentario = new Comentario();
        comentario = snapshot.val()
        comentario.key =  snapshot.key
        this.comentarios.unshift(comentario)
      }
    )
   
  }

  ionViewDidLoad() {
   
  }

  insertComment(){
    this.comentario.promoKey = this.promo.key
    this.comentario.texto = this.formComent.get('comment').value
    this.comentario.userUid = firebase.auth().currentUser.uid
    this.comentario.dataHora = Date.now()
    firebase.database().ref("comentarios").push(this.comentario)
    .then( () => {
      this.formComent.reset()
    })

   
  }

  removeComment(){

  } 

}
