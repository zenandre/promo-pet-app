import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComentsPage } from './coments';
import { ComponentsModule } from './../../components/components.module';

@NgModule({
  declarations: [
    ComentsPage,
  ],
  imports: [
  IonicPageModule.forChild(ComentsPage),
    ComponentsModule
  ],
})
export class ComentsPageModule {}
