import { Component, EventEmitter, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { MapsService } from './../../services/maps.service';
import {googlemaps} from 'googlemaps';
import { Geolocation } from '@ionic-native/geolocation';




@IonicPage()
@Component({
  selector: 'page-search-place',
  templateUrl: 'search-place.html',
})
export class SearchPlacePage {
  map: any;
  GoogleAutocomplete: any;
  GooglePlaces: any;
  geocoder: any;
  autocompleteItems: any = new Array<any>();
  nearbyItems: any = new Array<any>();
  autocomplete: string;
  localSelecionado: boolean = false;

  constructor(public zone: NgZone,
              public navCtrl: NavController,
              public navParams: NavParams,
              public mapsService: MapsService,
              public geolocation: Geolocation,
              public platform: Platform) {

                this.geocoder = new google.maps.Geocoder;
                this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
                this.autocompleteItems = [];

                this.platform.ready().then(
                  () => {
                    let geolocationOptions = {
                      enableHighAccuracy: true,
                      timeout: 5000,
                      maximumAge: 0
                    };

                    this.geolocation.getCurrentPosition(geolocationOptions).then(
                      coordenadas =>{
                        this.map = new google.maps.Map(document.getElementById('map'), {
                          center: {lat: coordenadas.coords.latitude, lng: coordenadas.coords.longitude},
                          zoom: 15
                        })
                      }
                    ).catch(
                      error => {
                        alert(JSON.stringify(error))
                      }
                    )  

                  }
                )
  }

  ionViewDidEnter(){
   
  
}

ionViewWillLeave(){
  if (!this.localSelecionado){
    this.mapsService.selectedPlaceEmitter.emit(null)
  }
}

  updateSearchResults() {
    if (this.autocomplete == "") {
      this.autocompleteItems = [];
      return;
    }
    this.GooglePlaces
    this.GoogleAutocomplete.getPlacePredictions(
      { input: this.autocomplete },
      (predictions, status) => {
        this.autocompleteItems = [];
        
          if (predictions != null){
          predictions.forEach(prediction => {
            this.autocompleteItems.push(prediction); 
          });}
        
      }
    );
  }

  selectSearchResult(local){
    this.autocompleteItems = []
    this.localSelecionado = true
    this.mapsService.selectedPlaceEmitter.emit(local)
    this.navCtrl.pop()
  }

}
