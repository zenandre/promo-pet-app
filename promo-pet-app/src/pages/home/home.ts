import { Component, OnInit } from '@angular/core';
import { NavController, IonicPage, MenuController, LoadingController, Loading, Platform } from 'ionic-angular';
import { AuthService } from './../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import firebase from 'firebase';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  
  loginForm: FormGroup;
  loading: Loading;

  constructor(public navCtrl: NavController,
              public menuController: MenuController,
              private authService: AuthService,
              public fb: FormBuilder,
              public loadingControler: LoadingController,
              public alertController: AlertController) {

                this.loginForm = this.fb.group({
                  email: ['',[Validators.required,Validators.email]],
                  senha: ['',[Validators.required]]
                })
               this.loading = this.loadingControler.create({
                 content: "Aguarde..."
               }) 
  }

  ngOnInit(){
    if (firebase.auth().currentUser){
      this.navCtrl.setRoot("TimelinePage")
    }
  }

  ionViewWillEnter(){
   this.menuController.swipeEnable(false);
  }

  ionViewDidLeave(){
    this.menuController.swipeEnable(true);

  }
  login(){
    this.loading.present();
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then( ()=> {
      firebase.auth().signInWithEmailAndPassword(this.loginForm.get('email').value,this.loginForm.get('senha').value)
      .then(value => { 
        this.loading.dismiss();     
        this.navCtrl.setRoot("TimelinePage")
      })
      .catch(err => {
        this.loading.dismiss();
        this.handleError(err)
      });
    }
     )
     .catch()
    
  }

  singUpEmail(){
    this.loading.present()
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then( ()=> {
    this.authService.signupEmail(this.loginForm.get('email').value,this.loginForm.get('senha').value)
    .then(value => {
      this.loading.dismiss()
      this.navCtrl.setRoot("UserDetailsPage")
    })
    .catch(err => {
      this.loading.dismiss()
      this.handleError(err)
    })
  }  ).catch()
  }
 

  handleError(error){
    let alertConf = null

    switch(error.code){
      case 'auth/user-not-found':
        alertConf = {
          title: 'Usuario não Cadastrado!',
          subTitle: 'Se você quiser pode manter os dados e clicar em cadastrar',
          buttons: ['Ok']
      }
      break

      case 'auth/wrong-password':
      alertConf = {
        title: 'Senha Inválida!',
        subTitle: 'A senha informada não está correta, tente novamente ou solicite a recuperação.',
        buttons: ['Ok']
       }
      break

      case 'auth/email-already-in-use':
      alertConf = {
        title: 'Email inválido!',
        subTitle: 'Este email já está em uso. Tente outro email para cadastrar-se.',
        buttons: ['Ok']
       }
      break
    }
    if (alertConf !=  null){
    let alert = this.alertController.create(alertConf)
    alert.present()
    }
  }

}
 