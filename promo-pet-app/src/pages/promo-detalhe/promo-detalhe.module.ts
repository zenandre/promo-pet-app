import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromoDetalhePage } from './promo-detalhe';

@NgModule({
  declarations: [
    PromoDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(PromoDetalhePage),
  ],
})
export class PromoDetalhePageModule {}
