import { Component, NgZone, OnInit } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from "ionic-angular";
import { Promocao } from './../../model/promocao.model';
import { PhotoService } from './../../services/photo.service';
import firebase from 'firebase';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MapsService } from './../../services/maps.service';
import { EstadoPromo } from "../../model/estado-promo.enum";
import { generateKey } from "../../services/util";


@IonicPage()
@Component({
  selector: "page-promo-detalhe",
  templateUrl: "promo-detalhe.html"
})
export class PromoDetalhePage {
 
  promo: Promocao = new Promocao();
  promoForm: FormGroup;
  photoTemp: string;
  loader: Loading;
  estadoPromo: EstadoPromo = EstadoPromo.NOVA;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public photoService: PhotoService,
    public fb: FormBuilder,
    public alertController: AlertController,
    public mapsService: MapsService,
    public loadController: LoadingController
  ) {
   
    this.promoForm = this.fb.group({
      descricao: ['',[Validators.required]],
      dataInicio: [''],
      dataFim: [''],
      local: ['',[Validators.required]],
      preco: ['',Validators.required]
    })

    this.mapsService.selectedPlaceEmitter.subscribe(
      local => {
        this.estadoPromo = EstadoPromo.NOVA
        if (local != null){
        this.promo.local = local
        this.promoForm.get('local').setValue(local.description)
        this.promoForm.get('local').markAsTouched()
        }
      },
      error => {
        console.log(error)
      }
    )
  }
ionViewWillLeave(){
  if (this.estadoPromo == EstadoPromo.NOVA
     && this.promo.fileNameStorage !=null
      && this.promo.fileNameStorage != undefined){
    this.loadingOn("Deletando foto da nuvem...")
    if (this.promo.fileNameStorage !=null && this.promo.fileNameStorage != undefined)
      this.deletePhotoFromStorage()
    }    
}

  loadingOn(mensagem: string){
    this.loader = this.loadController.create({
      content: mensagem
    })
    this.loader.present()
  }
  loadingOff(){
    this.loader.dismiss()
  }
  

  getCameraPicture(){
    if(this.promo.fileNameStorage !=null && this.promo.fileNameStorage != undefined){
      this.loadingOn("Deletando foto anterior...")
      this.deletePhotoFromStorage()
    }
    this.photoService.getCameraPicture().then(
      image => {
        this.photoTemp = 'data:image/jpeg;base64,' + image;
      }
    )
    .then(
      () => {
        this.uploadPromoPhoto();
      }
    )
    .catch(
    )
  }

  getGaleryPicture(){
    if(this.promo.fileNameStorage !=null && this.promo.fileNameStorage != undefined){
      this.loadingOn("Deletando foto anterior...")
      this.deletePhotoFromStorage()
    }
    this.photoService.getGaleryPicture().then(
      image => {
        this.photoTemp = 'data:image/jpeg;base64,' + image;        
      }
    )
    .then(
      () => {
        this.uploadPromoPhoto();
      }
    )
    .catch(
    )
  }

  uploadPromoPhoto(){
    this.loadingOn("Salvando foto na nuvem...")
    this.promo.fileNameStorage = generateKey();
    let uploadTask = this.photoService.uploadFoto(this.photoTemp,'promo_images',this.promo.fileNameStorage);
    uploadTask.on(
      'state_changed',
      (snapshot) => { //progresso
       },
      error => { //erro 
        this.loadingOff()
      },
      //sucesso
      () =>{
      firebase.storage().ref('promo_images').child(`${this.promo.fileNameStorage}.jpeg`).getDownloadURL().then(
       data => {
         this.loadingOff()
         this.promo.photoUrl =  data;
       }
     )
     .catch(
       error => {
        this.promo.photoUrl = null;
     })
      }
    )
  }

  savePromo(){
    this.loadingOn('Salvando promo...')    
    this.promo.userUid = firebase.auth().currentUser.uid;
    this.promo.descricao = this.promoForm.get('descricao').value
    this.promo.dataInicio = this.promoForm.get('dataInicio').value
    this.promo.preco = this.promoForm.get('preco').value
    this.promo.dataFim = this.promoForm.get('dataFim').value
    this.promo.dataCadastro = Date.now()
    firebase.database().ref("promo").push(this.promo)
    .then( () => {
      this.loadingOff()
      this.estadoPromo = EstadoPromo.SALVA
      this.navCtrl.setRoot("TimelinePage")
    })
    


  }

  limparForm(){
    this.promoForm.reset()
  }
 

  goToSelectPlace(){
    this.estadoPromo = EstadoPromo.MAPA
    this.navCtrl.push('SearchPlacePage');
  }

  deletePhotoFromStorage(){
    firebase.storage().ref().child(`promo_images/${this.promo.fileNameStorage}.jpeg`).delete()
      .then( () => {
        this.loadingOff()
        this.promo.fileNameStorage = null
        this.photoTemp = null;
      }
      )
      .catch(
        error => {
          this.loadingOff()
        }
      )
  }
  
}
