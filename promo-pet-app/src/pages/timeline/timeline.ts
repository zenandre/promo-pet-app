import { Component, OnInit } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  LoadingController,
  Loading,
  AlertController
} from "ionic-angular";
import { Promocao } from "../../model/promocao.model";
import firebase from "firebase";
import { Comentario } from "../../model/comentario.model";

@IonicPage()
@Component({
  selector: "page-timeline",
  templateUrl: "timeline.html"
})
export class TimelinePage implements OnInit{
  promos: Promocao[] = new Array<Promocao>();
  loader: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastController: ToastController,
    public loadingController: LoadingController,
    public alertController: AlertController
  ) {
    this.loader = this.loadingController.create({
      content: "Carregando promos..."
    });
  }

  ngOnInit(){
    this.loader
      .present()
      .then(() => {
        firebase
          .database()
          .ref("promo")
          .orderByChild("nome")
          .on("child_added", snapshot => {            
            let promo: Promocao = snapshot.val();
            promo.key = snapshot.key;
            promo.comentarios = new Array<Comentario>()
            firebase.database().ref('comentarios').orderByChild('promoKey').equalTo(promo.key).on(
              'child_added',
              snapshot => {
                promo.comentarios.push(snapshot.val())                
              }
            )
            this.promos.unshift(promo);
          });
      })
      .then( () => {
        this.loader.dismiss();
      })
      .catch( () => {
        this.loader.dismiss()
      });
    
  }

  addPromo() {
    this.navCtrl.push("PromoDetalhePage");
  }

  getMapLink(id) {
    return `https://www.google.com/maps/search/?api=1&query=Google&query_place_id=${id}`;
  }

  like(promo: Promocao) {
    let userUid = firebase.auth().currentUser.uid;
    if (promo.curtidas == null || promo.curtidas == undefined) {
      promo.curtidas = new Array<string>();
      promo.curtidas.push(userUid);
      firebase
        .database()
        .ref(`promo/${promo.key}`)
        .set(promo)
        .catch(error => {
          console.log(error);
        });
    } else if (promo.curtidas.indexOf(userUid) == null) {
      promo.curtidas.push(firebase.auth().currentUser.uid);
      firebase
        .database()
        .ref()
        .child(`promo/${promo.key}/curtidas`)
        .set(promo.curtidas)
        .then(() => {
        })
        .catch(error => {
        });
    } else {
      const toast = this.toastController.create({
        message: "Você já curtiu esta promo",
        duration: 2000,
        position: "top"
      });
      toast.present();
    }
  }

  negativa(promo: Promocao){
    let userUid = firebase.auth().currentUser.uid;
    if (promo.listaNegativacao == null || promo.listaNegativacao == undefined) {
      promo.listaNegativacao = new Array<string>();
      promo.listaNegativacao.push(userUid);
      firebase
        .database()
        .ref(`promo/${promo.key}`)
        .set(promo)
        .catch(error => {
        });
    } else if (promo.listaNegativacao.indexOf(userUid) == null) {
      promo.listaNegativacao.push(firebase.auth().currentUser.uid);
      firebase
        .database()
        .ref()
        .child(`promo/${promo.key}/listaNegativacao`)
        .set(promo.listaNegativacao)
        .then(() => {
        })
        .catch(error => {
        });
    } else {
      const toast = this.toastController.create({
        message: "Você já informou que esta promoção não é vantajosa.",
        duration: 3000,
        position: "top"
      });
      toast.present();
    }
  }

  comment(promo: any){
    this.navCtrl.push("ComentsPage",{promo: promo})
  }
 
  deletePromo(promoKey,i){
    let alert = this.alertController.create(
      {
        title: 'Excluir Promoção?',
        message: 'Tem certeza que quer excluir esta PROMO. A operação não poderá ser desfeita.',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel'           
          },
          {
            text: 'Excluir',
            handler: () => {
              firebase.database().ref().child(`promo/${promoKey}`).remove().then(
                () => {
                  this.promos.splice(i,1)
                }
              )
            }
          }
        ]
      }
    )
      alert.present()
  }

  podeExcluir(uid): boolean{
    if (uid == firebase.auth().currentUser.uid){
      return true
    }
    return false
  }
  
}
