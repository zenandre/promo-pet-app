import { DateTime } from "ionic-angular";


export class Promocao {
    constructor(
        public key: string = null,
        public descricao: string = null,
        public photoUrl: string = null,
        public preco: number = null,
        public userUid: string = null,
        public ativa: boolean = null,
        public listaNegativacao: string[] = new Array<string>(),
        public curtidas: string[] = new Array<string>(),
        public dataInicio: Date = null,
        public dataFim: Date = null,
        public dataCadastro: any = null,
        public comentarios: any[] = new Array<string>(),
        public local: any = {description: ""},
        public fileNameStorage: string = null
    ){
        

    }
}