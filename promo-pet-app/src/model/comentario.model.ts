
export class Comentario {
    constructor(
        public userUid: string = null,
        public texto: string = null,
        public key: string = null,
        public promoKey: string = null,
        public dataHora: any = null
    ){

    }
}