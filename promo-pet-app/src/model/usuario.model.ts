export class Usuario {
    constructor(
        public id: string = null,
        public nome: string = null,
        public photoUrl: string = null        
    ){

    }
}