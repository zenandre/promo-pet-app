import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { AuthService } from './../services/auth.service';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { FIREBASE_CONF } from './../config/firebase.config';
import { ErrorInterceptorProvider } from '../interceptors/error.interceptor';
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { PhotoService } from '../services/photo.service';
import { ComponentsModule } from './../components/components.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MapsService } from './../services/maps.service';

@NgModule({
  declarations: [
    MyApp
    
  ],
  imports: [
  BrowserModule,
    IonicModule.forRoot(MyApp,{
      monthNames: ['janeiro', 'fevereiro', 'março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro'],
      monthShortNames: ['jan', 'fev', 'mar','abr','mai','jun','jul','ago','set','out','nov','dez'],
      dayNames: ['domingo', 'segunda-feira', 'terça-feira','quarta-feira','quinta-feira','sexta-feira','sabado'],
      dayShortNames: ['dom', 'seg', 'ter','qua','qui','sex','sab',]
    }),
    AngularFireModule.initializeApp(FIREBASE_CONF),
    AngularFireAuthModule,
    ComponentsModule,
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp      
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    ErrorInterceptorProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    Geolocation,
    PhotoService,
    MapsService
       
  ]
})
export class AppModule {}
