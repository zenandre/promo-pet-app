import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService } from './../services/auth.service';
import firebase from 'firebase';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = '';

  pages: Array<{title: string, component: string,type: string,icon:string}>;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public authService: AuthService) {
    this.initializeApp();

    this.pages = [
      { title: 'Informações do Usuario', component: 'UserDetailsPage',type: 'push',icon: 'people'  }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    this.authService.firebaseAuth.authState.subscribe(
      user => {
        if (user) {
          this.rootPage = 'TimelinePage'
        } else {
          this.rootPage = 'HomePage'
        }
      },
      () => {
        this.rootPage = 'HomePage'
      }
    )

  }

  openPage(page) {
    if(page.type == 'setRoot'){
    this.nav.setRoot(page.component);
    }
    else {
      this.nav.push(page.component);
    }
  }

  logout(){
    firebase.auth().signOut().then(
      () => {
        this.nav.setRoot("HomePage")
      }
    )
  }
}
