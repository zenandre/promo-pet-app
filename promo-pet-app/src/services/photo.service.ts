import { Injectable } from "@angular/core";
import firebase  from 'firebase';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Injectable()
export class PhotoService {
    constructor(public camera: Camera){

    }

    uploadFoto(foto,referencia,nomeArquivo): firebase.storage.UploadTask{
       return firebase.storage().ref(`${referencia}/${nomeArquivo}.jpeg`).putString(foto,'data_url')     
      }

      getGaleryPicture(): Promise<any>{  
       return this.camera.getPicture(this.getGaleryOptions())
      }
    
      getCameraPicture(): Promise<any>{            
      return  this.camera.getPicture(this.getCameraOptions())
      }


      getCameraOptions(){
        const cameraOptions: CameraOptions = {
          quality: 50,
          sourceType: this.camera.PictureSourceType.CAMERA,
          destinationType: this.camera.DestinationType.DATA_URL,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          allowEdit: true,
          correctOrientation: true
        }
        return cameraOptions;
      }
      
      getGaleryOptions(){
        const galeryOptions: CameraOptions = {
          quality: 50,
          sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
          destinationType: this.camera.DestinationType.DATA_URL,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          allowEdit: true,
          correctOrientation: true
        }
        return galeryOptions;
      }
}