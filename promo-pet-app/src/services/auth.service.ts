import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {
  user: Observable<firebase.User>;

  constructor(public firebaseAuth: AngularFireAuth
              ) {
    this.user = firebaseAuth.authState;
  }

  signupEmail(email: string, password: string) {
   return this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(email, password)        
  }

  loginEmail(email: string, password: string) {
   return this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password);
  }

  logout() {
    this.firebaseAuth
      .auth
      .signOut();
  }

}