export const generateKey = () => {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (let i = 0; i < 19; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
}