import { NgModule } from '@angular/core';
import { MiniAvatarComponent } from './mini-avatar/mini-avatar';
import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
@NgModule({
	declarations: [MiniAvatarComponent],
	imports: [CommonModule,IonicModule],
	exports: [MiniAvatarComponent]
})
export class ComponentsModule {}
