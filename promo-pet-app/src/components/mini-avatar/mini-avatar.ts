import { Component, Input, OnInit } from '@angular/core';
import firebase from 'firebase'
import { Usuario } from './../../model/usuario.model';

@Component({
  selector: 'mini-avatar',
  templateUrl: 'mini-avatar.html'
})
export class MiniAvatarComponent implements OnInit {

  user: Usuario = new Usuario();
  @Input() uid: string;

  constructor() {
    
  }

  ngOnInit(){
    firebase.database().ref('/user_data/'+ this.uid).once('value')
    .then(
      snapshop => {
        this.user = snapshop.val()
      }
    ) 
    .catch(
      error => {
      }
    )
  }
}
